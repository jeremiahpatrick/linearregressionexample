#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QString>
#include <QComboBox>
#include <QGenericMatrix>
#include <QVector>
#include "datahub.h"
#include <cfloat>

/// DataHub constructor
///
/// This constructor will initialize the DataHub object-
/// the main object from which new data is loaded, chart updates
/// are ordered, and regressions are performed.
///
/// The QWidget objects housed in DataHub are present in order to
/// make updating the GUI a faster and simpler process.
DataHub::DataHub(QWidget *parent)
{
    _dataSet = new QList<QPointF>();
    _chartView = new ChartView(parent);

    _lineRegressionEquation = new QLineEdit("y = ");
    _lineRegressionEquation->setReadOnly(true);
    _spinBoxOrder = new QSpinBox(parent);
    _spinBoxOrder->setMaximum(7);
    _spinBoxOrder->setMinimum(1);
    _spinBoxOrder->setValue(1);
}

DataHub::~DataHub()
{
    delete _dataSet;
    delete _chartView;
    delete _lineRegressionEquation;
    delete _spinBoxOrder;
}

/// Load Data
///
/// This function will open a csv with two columns of numbers and
/// load the information into the _dataSet object.
///
/// Upon completion of loading the object, the data will be plotted
/// on the scatterplot.
void DataHub::LoadData()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open CSV"), "",
                                                    tr("Comma-Separated Values (*.csv);;All Files (*)"));
    if (fileName.isEmpty())
        return;
    else
    {

        QFile file(fileName);

        if (!file.open(QIODevice::ReadOnly))
        {
            QMessageBox::information(this, tr("Unable to open file"), file.errorString());
            return;
        }

        QTextStream in(&file);
        _dataSet->clear(); // Clear dataset so new data can be loaded

        while (!in.atEnd()) {

            QString line = in.readLine();
            QStringList list = line.split(",");

            // Only accept two values per line format
            if (list.length() == 2)
            {
                QPointF pt(list[0].toDouble(), list[1].toDouble());
                _dataSet->push_back(pt);
            }
        }

        // No data points loaded
        if (_dataSet->length() == 0)
        {
            QMessageBox::warning(this, tr("Error in loading data"), tr("No data loaded. Check to make sure that your data is formatted in two columns separated by a comma."));
            return;
        }

        // Update the chart object now
        _chartView->UpdateChart(_dataSet);

    }

    _lineRegressionEquation->setText("y = ");

}

/// Getter for the Chart View Object
ChartView* DataHub::GetChartView()
{
    return _chartView;
}

/// Getter for the Polynomial order Text box
QLineEdit* DataHub::GetLinRegTextLine()
{
    return _lineRegressionEquation;
}

/// Getter for the Polynomial order spinbox
QSpinBox* DataHub::GetSpinBox()
{
    return _spinBoxOrder;
}

/// Perform Regression
///
/// This function is called when the user clicks on the 'Perform Regression'
/// button.
///
/// First, the data set will be examined to ensure that there is data loaded
/// and that there is enough data to be analyzed.
///
/// Second, the order of the regression will be examined. If a first order
/// regression has been requested (Linear) a Linear Regression will be performed.
/// Otherwise a N degree regression will be performed, where N is the value in
/// the spinbox.
void DataHub::PerformRegression()
{
    if (_dataSet->size() == 0) {
        QMessageBox::warning(this, "No data loaded", "No data has been loaded. Please load a data set.");
        return;
    }
    else if (_dataSet->size() == 1) {
        QMessageBox::warning(this, "Too small of data set", "There is only one data point loaded in this set. Please load a data set with more than one point.");
        return;
    }

    if (_spinBoxOrder->value() == 1)
        PerformLinearRegression();
    else
        // The order requested needs to be bumped by 1 in order to
        // request an accurate order equation
        PerformPolynomialRegression(_spinBoxOrder->value());

}

/// Perform Linear Regression
///
/// This will perform a linear regression on the loaded data points
/// and display a first order equation (y=mx+b) on the output.
///
/// After finding the equation, it will be plotted on the chart
///
/// code gotten from http://david.swaim.com/cpp/linreg.htm
void DataHub::PerformLinearRegression()
{
    double n = _dataSet->length(); // number of data points input so far
    double sumX = 0, sumY = 0;  // sums of x and y
    double sumXsquared = 0, // sum of x squares
            sumYsquared = 0; // sum y squares
    double sumXY = 0;       // sum of x*y

    double a = 0, b = 0;        // coefficients of f(x) = a + b*x
    double coefD = 0,       // coefficient of determination
            coefC = 0,       // coefficient of correlation
            stdError = 0;    // standard error of estimate

    // Load up all the data
    for (QPointF val : *_dataSet)
    {
        sumX += val.rx();
        sumY += val.ry();
        sumXsquared += val.rx() * val.rx();
        sumYsquared += val.ry() * val.ry();
        sumXY += (val.rx() * val.ry());
    }


    if (fabs( double(n) * sumXsquared - sumX * sumX) > DBL_EPSILON)
    {
        b = ( double(n) * sumXY - sumY * sumX) /
                ( double(n) * sumXsquared - sumX * sumX);
        a = (sumY - b * sumX) / double(n);

        double sx = b * ( sumXY - sumX * sumY / double(n) );
        double sy2 = sumYsquared - sumY * sumY / double(n);
        double sy = sy2 - sx;

        coefD = sx / sy2;
        coefC = sqrt(coefD);
        stdError = sqrt(sy / double(n - 2));
    }
    else
    {
        a = b = coefD = coefC = stdError = 0.0;
    }

    QString linRegResult = tr("y = %1 + %2 x").arg(QString::number(a), QString::number(b));

    _chartView->DrawRegressionLine(a, b);

    _lineRegressionEquation->setText(linRegResult);

}


/// Perofrm Polynomial Regression
///
/// This will perform a n-degree polynomial regression on the loaded
/// data points using Gaussian Elimination. After performing the
/// regression, the equation will be plotted on the chart and also
/// printed in the text box for the user to copy.
///
/// Sources:
/// https://arachnoid.com/sage/polynomial.html
/// http://www.bragitoff.com/2015/09/c-program-for-polynomial-fit-least-squares/
void DataHub::PerformPolynomialRegression(int degree)
{
    // Increase the value for degree by one so that x^0 is accounted for
    // when we generate our end polynomial constants.
    degree++;

    // Setup -----
    double normalMatrix[degree][degree + 1];
    double matrixY[degree + 1];
    // This is a vector because we're going to pass it later on, and there's no consistent size of the array.
    QVector<double> polyConstants;
    polyConstants.resize(degree + 1);

    //sigmaValues is the array that will store the values of sigma(xi),sigma(xi^2),sigma(xi^3)....sigma(xi^2n)
    double sigmaValues[2 * degree + 1];

    for (int u = 0; u < 2*degree+1; u++)
        sigmaValues[u] = 0;

    for (int u = 0; u < degree + 1; u++) {
        matrixY[u] = 0;
        polyConstants[u] = 0;
    }

    for (int u = 0; u < degree; u++)
        for (int v = 0; v < degree + 1; v++)
            normalMatrix[u][v] = 0;

    // Load Variables -----
    // run through all input values
    for (QPointF point : *_dataSet)
    {
        // generate sigma(x) values
        for (int i = 0; i < (2 * degree + 1); i++)
        {
            double temp = pow(point.rx(), i);
            sigmaValues[i] += temp;
        }

        // generate sigma(x)*y values
        for (int i = 0; i < (degree + 1); i++)
            matrixY[i] += pow(point.rx(), i) * point.ry();
    }

    // Fill the normalMatrix with the sigma values
    for (int row = 0; row < degree; row++)
        for (int col = 0; col < degree; col++)
            normalMatrix[row][col] = sigmaValues[row + col];

    // Load the values of Y as the last column of the Augmented Normal Matrix
    for (int i = 0; i < degree; i++)
        normalMatrix[i][degree] = matrixY[i];

    // Pivotization -----
    //From now Gaussian Elimination starts(can be ignored) to solve the set of linear equations (Pivotisation)
    for (int i = 0; i < degree; i++)
        for (int k = i + 1; k < degree; k++)
            if (normalMatrix[i][i] < normalMatrix[k][i])
                for (int j = 0; j <= degree; j++)
                {
                    double temp = normalMatrix[i][j];
                    normalMatrix[i][j] = normalMatrix[k][j];
                    normalMatrix[k][j] = temp;
                }

    // Gaussian Eliminiation -----
    for (int i = 0; i < degree; i++)
        for (int k= 0; k < degree; k++)
            if (i != k)
            {
                double t = normalMatrix[k][i]/normalMatrix[i][i];
                for (int j = 0; j <= degree; j++)
                    //make the elements below and above the pivot elements equal to zero or elimnate the variables
                    normalMatrix[k][j] = normalMatrix[k][j] - t * normalMatrix[i][j];
            }

    // reduce the pivot elements to 1
    for (int i = 0; i < degree; i++)
    {
        double temp = 1.0 / normalMatrix[i][i];
        normalMatrix[i][i] *= temp;
        normalMatrix[i][degree] *= temp;
    }

    // get the polynomial constants
    for (int i = 0; i < degree; i++)
        polyConstants[i] = normalMatrix[i][degree];

    // Draw the regression line on the chart
    _chartView->DrawPolynomialRegressionLine(polyConstants);

    // generate the equation to be shown
    QString output = "y = ";
    for (int i = 0; i < polyConstants.size(); i++)
    {
        if (polyConstants[i] != 0)
        {
            if (i != 0)
                output += " + ";

            output += QString::number(polyConstants[i]);
            if (i == 1)
                output += " x";
            else if (i != 0)
                output += " x^" + QString::number(i);
        }

    }

    // Show the equation
    _lineRegressionEquation->setText(output);
}

