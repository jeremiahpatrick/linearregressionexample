#-------------------------------------------------
#
# Project created by QtCreator 2018-06-20T20:29:27
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StatisticsStuff
TEMPLATE = app


SOURCES += \
    main.cpp \
    chartview.cpp \
    datahub.cpp

HEADERS  += \
    chartview.h \
    datahub.h

FORMS    +=
