#ifndef CHARTVIEW_H
#define CHARTVIEW_H


#include <QtCharts/QChartGlobal>
#include <QtCharts/QChartView>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QLineSeries>
#include <QtCore/QtMath>
#include <QList>


QT_CHARTS_USE_NAMESPACE

class ChartView : public QChartView
{
    Q_OBJECT

public:
    ChartView(QWidget *parent = 0);
    ~ChartView();

    void UpdateChart( QList<QPointF> * _update);
    void DrawRegressionLine(double intercept, double slope);
    void DrawPolynomialRegressionLine(QVector<double>& equation);

private Q_SLOTS:
//    void handleClickedPoint(const QPointF &point);

private:
    QScatterSeries *_scatter;
    QLineSeries *_lineSeries;
    double maxX, minX, maxY, minY;

};

#endif // CHARTVIEW_H
