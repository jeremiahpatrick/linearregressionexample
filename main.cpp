#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QGridLayout>
#include <QPushButton>
#include <QSpinBox>
#include <QLabel>
#include "datahub.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QMainWindow window;

    DataHub dataHub(&window);

    QGridLayout *gridLayout = new QGridLayout;
    QPushButton *buttonLoadDataSet = new QPushButton("Load Data Set");
    QPushButton *buttonDoLinearRegression = new QPushButton("Execute Regresssion");
    QLabel      *labelDescription = new QLabel("Order Polynomial Regression:");
    labelDescription->setAlignment(Qt::AlignRight);


    QWidget::connect(buttonLoadDataSet, QPushButton::pressed, &dataHub, DataHub::LoadData);
    QWidget::connect(buttonDoLinearRegression, QPushButton::pressed, &dataHub, DataHub::PerformRegression);

    gridLayout->addWidget(buttonLoadDataSet,        0, 0, 1, 1);
    gridLayout->addWidget(buttonDoLinearRegression, 0, 1, 1, 1);
    gridLayout->addWidget(labelDescription,         0, 2, 1, 2);
    gridLayout->addWidget(dataHub.GetSpinBox(),     0, 4, 1, 1);

    gridLayout->addWidget(dataHub.GetLinRegTextLine(), 1, 0, 1, 5);

    gridLayout->addWidget(dataHub.GetChartView(),   2, 0, 5, 5);

    QWidget *w = new QWidget();

    w->setLayout(gridLayout);
    w->setWindowTitle("Regression Example Window");
    w->show();

    return a.exec();

    delete gridLayout;
    delete buttonDoLinearRegression;
    delete buttonLoadDataSet;
    delete labelDescription;
    delete w;

    return 1;
}
