#ifndef DATAHUB_H
#define DATAHUB_H

#include <QList>
#include <QWidget>
#include <QLineEdit>
#include <QSpinBox>
#include <QtCore/QtMath>
#include "chartview.h"

class DataHub : public QWidget
{
    Q_OBJECT

public:

    DataHub(QWidget *parent = 0);
    ~DataHub();
    ChartView* GetChartView();
    QLineEdit* GetLinRegTextLine();
    QSpinBox* GetSpinBox();

public Q_SLOTS:
    void LoadData();
    void PerformRegression();
    void PerformLinearRegression();
    void PerformPolynomialRegression(int degree);

private:

    ChartView* _chartView;
    QList<QPointF>* _dataSet;
    QLineEdit* _lineRegressionEquation;
    QSpinBox* _spinBoxOrder;

};

#endif // DATAHUB_H
