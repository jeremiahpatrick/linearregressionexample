#include "chartview.h"
#include <QtCore/QtMath>

QT_CHARTS_USE_NAMESPACE


/// ChartView Constructor
///
/// This constructor will initialize the ChartView object
/// and prep the Scatter series and line series objects to
/// be ready for plotting data.
ChartView::ChartView(QWidget *parent)
    : QChartView(new QChart(), parent),
      _scatter(0)
{
    setRenderHint(QPainter::Antialiasing);

    chart()->setTitle("ScatterPlot");

    _scatter = new QScatterSeries();
    _lineSeries = new QLineSeries();

    chart()->addSeries(_scatter);
    chart()->addSeries(_lineSeries);
    chart()->createDefaultAxes();
    chart()->legend()->hide();
}

ChartView::~ChartView() {
    delete _scatter;
    delete _lineSeries;
}

/// Update Chart
///
/// This function is called after a new dataset is loaded.
///
/// It begins by clearing the old data sets for the scatterplot
/// and the regression line.
///
/// Next, the points are added to the scatter plot. As the
/// points are loaded, the min and max values of the x and y
/// dimensions are found.
///
/// Finally, the chart is resized so that all the points are visible.
///
void ChartView::UpdateChart(QList<QPointF>* _update)
{
    // Don't check for length 0 - that should have already happened
    minX = 0, minY = 0, maxY = 0, maxX = 0;

    _scatter->clear();
    _lineSeries->clear();

    for (auto it = _update->begin(); it != _update->end(); ++it)
    {

        // Get the min and max of these
        if (it->rx() > maxX) maxX = it->rx();
        if (it->rx() < minX) minX = it->rx();
        if (it->ry() > maxY) maxY = it->ry();
        if (it->ry() < minY) minY = it->ry();

        _scatter->append(it->toPoint());
    }

    // Add 5% to each or increase the magnitude by 1 if less than 20 so
    // all the points are easily visible, and round so that the numbers
    // on the axes look nice
    chart()->axisX()->setRange(round(minX < 20 ? minX - 1 : minX*1.05), round(maxX < 20 ? maxX + 1 : maxX*1.05));
    chart()->axisY()->setRange(round(minY < 20 ? minY - 1 : minY*1.05), round(maxY < 20 ? maxY + 1 : maxY*1.05));
    chart()->createDefaultAxes();
}

/// Draw Linear Regression Line
///
/// In this function, plot a regression line for a first order equation-
///  that is, an equation of type y = mx + b. The slope and the intercept
///  are passed in and two points are calcuated and stored in the _lineSeries object
void ChartView::DrawRegressionLine(double intercept, double slope)
{
    // each time this function is run, clear the series. We only want a single
    //  regression equation shown at a time.
    _lineSeries->clear();

    double leftX, leftY;
    double rightX, rightY;

    leftX = maxX * .1;
    leftY = intercept + (slope * maxX * .1);

    rightX = maxX * .9;
    rightY = intercept + (slope * maxX * .9);

    _lineSeries->append(leftX, leftY);
    _lineSeries->append(rightX, rightY);

    // For whatever reason, if createDefaultAxes is not executed, then
    //  the line will not be rendered properly. Thus it must be run each time.
    chart()->createDefaultAxes();
}

/// Draw Polynomial Regression Line
///
/// This function takes a series of values in a vector of doubles. These are
/// coefficients which are then plotted over 80% of the x values on the chart.
///
/// This function is capable of plotting 0 order functions to N order functions.
void ChartView::DrawPolynomialRegressionLine(QVector<double>& equation)
{
    // each time this function is run, clear the series. We only want a single
    //  regression equation shown at a time.
    _lineSeries->clear();

    // Plot all generated points
    for (double x = .01; x <= .99; x+= .01)
    {
        double y = 0;
        for (double power = 0; power < equation.size(); power++)
            y += (equation[power] * pow(x * maxX, power));

        _lineSeries->append(x * maxX, y);
    }

    // For whatever reason, if createDefaultAxes is not executed, then
    //  the line will not be rendered properly. Thus it must be run each time.
    chart()->createDefaultAxes();
}
